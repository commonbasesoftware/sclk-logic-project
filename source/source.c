/*
 * source.c
 *
 *  Created on: 28.09.2021
 *      Author: A.Niggebaum
 *
 * Dummy file. Forces the compiler to include the libraries. All important functionality is defined in the libraries.
 * - Core_Base_Library: basic functionality of core
 * - ...logic		  : specific logic
 *
 */
